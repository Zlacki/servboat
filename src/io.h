/**
 * @file io.h
 * @author Zach Knight
 * @date 09 Oct 2013
 * @brief File declaring functions for I/O.
 *
 * This code declares what functions need to be shared with other
 * parts of the application, which are implemented in the file io.c
 * and includes header for boolean types for C.
 */

#ifndef _IO_H
#define _IO_H

#include <stdbool.h>
#include "util.h"

typedef struct {
	FILE *fp;
	char *in, *out;
} ipc_handle_t;

ipc_handle_t *ipc_handles;
int ipc_index;
char bufin[BUFFER_SIZE];
char bufout[BUFFER_SIZE];

void ipc_add_module(FILE *, char *, char *);
int ipc_read(ipc_handle_t);
int ipc_send(ipc_handle_t);
bool irc_connect(char *, unsigned int);
void irc_out(char *, ...);
int irc_read(char *);

#endif
