/**
 * @file util.h
 * @author Zach Knight
 * @date 09 Oct 2013
 * @brief File declaring various ‘peripheral’ functions and constants.
 *
 * This code declares any functions that do not really have enough
 * common code to fit into its own file, and are useful to the project
 * to simplify programming.  It also implements various constants to
 * simplify the programming as well.
 */

#ifndef _UTIL_H
#define _UTIL_H

#define BUFFER_SIZE 4096
#define DEBUG true

void eprint(const char *, ...);
int dial(char *, char *);
void strlcpy(char *, const char *, int);
char *eat(char *, int (*)(int), int);
char* skip(char *, char);
void trim(char *);
void *safe_alloc(size_t);
void *safe_calloc(size_t, size_t);
void *safe_realloc(void *, size_t);
char *strformat(const char *, ...);

#endif
