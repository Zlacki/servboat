/**
 * @file irc.h
 * @author Zach Knight
 * @date 09 Oct 2013
 * @brief File declaring an abstraction layer for the IRC protocol.
 *
 * This code declares a bunch of useful functions to make handling
 * the IRC protocol much easier on the programmer.  This will not
 * likely be used much because there will be one more abstraction layer
 * for implementing modules to communicate with this application.
 *
 * It also declares some IRC variables for identification, etc.
 */

#ifndef _IRC_H
#define _IRC_H

#define NICK "servboat"
#define USER "Serv the Boat"
#define SERVER "irc.yourstruly.sx"
#define PORT "6667"
#define CHANNEL "#friends"
#define COMMAND_PREFIX '.'

void irc_notice_event(char *, char *, char *);
void irc_welcome_event(void);
void irc_privmsg_event(char *, char *, char *);
void irc_privmsg(const char *, const char *);
void irc_join_channel(const char *);
void irc_notice(const char *, const char *);

#endif
