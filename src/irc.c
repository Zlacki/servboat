/**
 * @file irc.c
 * @author Zach Knight
 * @date 09 Oct 2013
 * @brief File implementing an abstraction layer for the IRC protocol.
 *
 * This code implements a bunch of useful functions to make handling
 * the IRC protocol much easier on the programmer.  This will not
 * likely be used much because there will be one more abstraction layer
 * for implementing modules to communicate with this application.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "irc.h"
#include "io.h"
#include "util.h"
#include "uno.h"

void irc_notice_event(char *sender, char *argument, char *content) {

}

void irc_welcome_event(void) {
	irc_join_channel(CHANNEL);
}

void irc_privmsg_event(char *sender, char *argument, char *content) {
	if(memchr(content, COMMAND_PREFIX, 1)) {
		char *args = skip(content, ' ');
		char *command = skip(content, '.');
		for(int i = 0; i < strlen(command); i++)
			if(command[i] == ' ')
				command[i] = '\0';
		int argc = 0;
		char **argv = NULL;
		if(args) {
			argc = 1;
			for (int i = 0; args[i]; i++)
				argc += (args[i] == ' ');
			argv = (char **) safe_calloc(argc, sizeof(char *));
			argv[0] = args;
			for(int i = 1; i <= argc; i++) {
				trim(argv[i - 1]);
				argv[i] = skip(argv[i - 1], ' ');
			}
		}
		if(!strncmp(command, "echo", 4) && argc > 0) {
			char *msg = safe_alloc(BUFFER_SIZE);
			msg[0] = '\0';
			for(int i = 0; i < argc; i++) {
				if(i > 0)
					strcat(msg, " ");
				strcat(msg, argv[i]);
			}
			irc_privmsg(argument, msg);
			free(msg);
		} else if(!strncmp(command, "unostart", 8) && !uno_is_started) {
			uno_start_game();
			irc_privmsg(argument, "Uno game started!  Type .join to play!");
			uno_add_player(sender);
			irc_privmsg(argument, strformat("%s has joined the Uno game!", sender));
			irc_privmsg(argument, strformat("Current card: %s %s", uno_color(uno_current_card), uno_type(uno_current_card)));
		} else if(!strncmp(command, "unostop", 7) && uno_is_started) {
			uno_stop_game();
			irc_privmsg(argument, "Uno game stopped.");
		} else if(!strncmp(command, "unojoin", 7) && uno_is_started && !uno_contains_player(sender)) {
			uno_add_player(sender);
			irc_privmsg(argument, strformat("%s has joined the Uno game!", sender));
		} else if(!strncmp(command, "unoquit", 7) && uno_is_started && uno_contains_player(sender)) {
			uno_remove_player(sender);
			irc_privmsg(argument, strformat("%s has quit the Uno game!", sender));
		} else if(!strncmp(command, "unohand", 7) && uno_is_started && uno_contains_player(sender)) {
			uno_player_t *player = uno_get_player(sender);
			char *msg = safe_alloc(BUFFER_SIZE);
			msg[0] = '\0';
			strcat(msg, "Cards: ");
			for(int i = 0; i < player->hand_count; i++)
				if(player->hand[i])
					strcat(msg, strformat("%s%s ", uno_color(player->hand[i]), uno_type(player->hand[i])));
			irc_notice(sender, msg);
			free(msg);
		} else if(!strncmp(command, "unoplay", 7) && uno_is_started && uno_contains_player(sender) && argc > 0) {
			char c = tolower(**argv);
			if(strchr(CARD_CHARS, c)) {
				int color = -1;
				if(c == 'r')
					color = RED;
				else if(c == 'g')
					color = GREEN;
				else if(c == 'b')
					color = BLUE;
				else if(c == 'y')
					color = YELLOW;
				else if(c == 'w')
					color = -2;
				c = tolower((*argv)[1]);
				int value = c - '0';
				if(value < 10 && value >= 0 && color != -2) {
					uno_player_t *player = uno_get_player(sender);
					for(int i = 0; i < player->hand_count; i++) {
						if(color == player->hand[i]->color && value == player->hand[i]->value) {
							if(uno_current_card->type >= NUMBER && uno_current_card->type < WILD && uno_current_card->color >= RED && uno_current_card->color <= YELLOW) {
								if(color == uno_current_card->color || value == uno_current_card->value || (uno_current_card->value == -2 && color == uno_current_card->color)) {
									irc_privmsg(argument, strformat("%s played %s %d.", sender, uno_color(player->hand[i]), player->hand[i]->value));
									irc_privmsg(argument, strformat("Current card: %s %d", uno_color(player->hand[i]), player->hand[i]->value));
									uno_discard(player, i);
									goto FREE_ARGS;
								} else {
									if(uno_current_card->value == -2)
										irc_privmsg(argument, strformat("Invalid play!  Card must be Wild or %s of any value.", uno_color(uno_current_card)));
									else
										irc_privmsg(argument, strformat("Invalid play!  Card must be Wild, %s of any value, or %d of any color.", uno_color(uno_current_card), uno_current_card->value));
									goto FREE_ARGS;
								}
							}
						}
					}
					irc_privmsg(argument, "You do not have that card!");
				} else if(color == -2 && strchr(CARD_CHARS, c)) {
					char *tmp = "";
					if(c == 'r') {
						color = RED;
						tmp = "Red";
					} else if(c == 'g') {
						color = GREEN;
						tmp = "Green";
					} else if(c == 'b') {
						color = BLUE;
						tmp = "Blue";
					} else if(c == 'y') {
						color = YELLOW;
						tmp = "Yellow";
					} else if(c == 'w') {
						irc_privmsg(argument, "Invalid play!  Color can not be set to Wild.");
						goto FREE_ARGS;
					}
					uno_player_t *player = uno_get_player(sender);
					for(int i = 0; i < player->hand_count; i++) {
						if(player->hand[i]->type == WILD/* || uno_current_card->type == WILD_DRAW_FOUR*/) {
							irc_privmsg(argument, strformat("%s played %s and set color to %s.", sender, uno_type(player->hand[i]), tmp));
							irc_privmsg(argument, strformat("Current card: Anything %s", tmp));
							uno_discard(player, i);
							uno_current_card->type = NUMBER;
							uno_current_card->color = color;
							uno_current_card->value = -2;
							goto FREE_ARGS;
						}
					}
					irc_privmsg(argument, "You do not have that card!");
				}
			}
		} else if(!strncmp(command, "unodraw", 7) && uno_is_started && uno_contains_player(sender)) {
			uno_player_t *player = uno_get_player(sender);
			uno_draw_card(player);
			irc_notice(sender, strformat("You drew a %s %s.", uno_color(player->hand[player->hand_count - 1]), uno_type(player->hand[player->hand_count - 1])));
		}
FREE_ARGS:
		if(argc > 0)
			free(argv);
	}
}

void irc_privmsg(const char *recipient, const char *message) {
	if(recipient[0] == '\0') {
		puts("irc_privmsg WARNING: No recipient specified.");
		return;
	}
	irc_out("PRIVMSG %s :%s", recipient, message);
	return;
}

void irc_notice(const char *recipient, const char *message) {
	if(recipient[0] == '\0') {
		puts("irc_notice WARNING: No recipient specified.");
		return;
	}
	irc_out("NOTICE %s :%s", recipient, message);
	return;
}

void irc_join_channel(const char *channel) {
	irc_out("JOIN %s", channel);
	return;
}
