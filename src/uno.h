#ifndef _UNO_H
#define _UNO_H

#include <stdbool.h>

#define TRUE 1
#define FALSE 0
#define CARD_CHARS "rgbyw"

typedef enum { RED, GREEN, BLUE, YELLOW } card_color_t;
typedef enum { NUMBER, SKIP, DRAW_TWO, REVERSE, WILD, WILD_DRAW_FOUR } card_type_t;

typedef struct {
	card_color_t color;
	card_type_t type;
	int value;
} uno_card_t;

typedef struct {
	char *name;
	uno_card_t **hand;
	int hand_count;
} uno_player_t;

uno_card_t **uno_deck;
uno_card_t *uno_current_card;
uno_player_t **uno_players;
bool uno_is_started;
int uno_player_count;

void uno_start_game();
void uno_stop_game();
int uno_get_random_card_index();
int uno_get_random_empty_card_index();
void uno_draw_card(uno_player_t *);
void uno_discard(uno_player_t *, int);
void uno_add_player(char *);
void uno_remove_player(char *);
bool uno_contains_player(char *);
uno_player_t *uno_get_player(char *);
const char *uno_color(uno_card_t *);
const char *uno_type(uno_card_t *);

#endif
