#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "uno.h"
#include "util.h"

void uno_start_game() {
	uno_card_t *card;

	uno_players = safe_calloc(256, sizeof(uno_player_t *));
	uno_deck = safe_calloc(108, sizeof(uno_card_t *));
	uno_player_count = 0;

	for(int i1 = 0; i1 < 4; i1++) {
		card_color_t color;

		switch(i1) {
		case 0:
			color = RED;
			break;
		case 1:
			color = GREEN;
			break;
		case 2:
			color = BLUE;
			break;
		case 3:
			color = YELLOW;
			break;
		default:
			color = -1;
			break;
		}

		card = safe_alloc(sizeof(uno_card_t *));
		card->color = color;
		card->type = NUMBER;
		card->value = 0;
		uno_deck[uno_get_random_empty_card_index()] = card;
		for(int i = 1; i < 10; i++) {
			for(int i2 = 0; i2 < 2; i2++) {
				card = safe_alloc(sizeof(uno_card_t *));
				card->color = color;
				card->type = NUMBER;
				card->value = i;
				uno_deck[uno_get_random_empty_card_index()] = card;
			}
		}

		for(int i = 0; i < 2; i++) {
			card = safe_alloc(sizeof(uno_card_t *));
			card->color = color;
			card->type = SKIP;
			card->value = -1;
			uno_deck[uno_get_random_empty_card_index()] = card;
			card = safe_alloc(sizeof(uno_card_t *));
			card->color = color;
			card->type = DRAW_TWO;
			card->value = -1;
			uno_deck[uno_get_random_empty_card_index()] = card;
			card = safe_alloc(sizeof(uno_card_t *));
			card->color = color;
			card->type = REVERSE;
			card->value = -1;
			uno_deck[uno_get_random_empty_card_index()] = card;
		}
	}

	for(int i = 0; i < 4; i++) {
		card = safe_alloc(sizeof(uno_card_t *));
		card->color = -2;
		card->type = WILD;
		card->value = -1;
		uno_deck[uno_get_random_empty_card_index()] = card;
		card = safe_alloc(sizeof(uno_card_t *));
		card->color = -2;
		card->type = WILD_DRAW_FOUR;
		card->value = -2;
		uno_deck[uno_get_random_empty_card_index()] = card;
	}

	int index = uno_get_random_card_index();
	uno_current_card = uno_deck[index];
	uno_deck[index] = NULL;
	uno_is_started = TRUE;
}

void uno_stop_game() {
	for(int i = 0; i < 108; i++)
		if(uno_deck[i])
			free(uno_deck[i]);
	free(uno_deck);

	for(int i = 0; i < uno_player_count; i++) {
		for(int i1 = 0; i1 < uno_players[i]->hand_count; i1++)
			free(uno_players[i]->hand[i1]);
		free(uno_players[i]->hand);
		free(uno_players[i]);
	}
	free(uno_players);

	uno_is_started = FALSE;
}

void uno_discard(uno_player_t *player, int index) {
	uno_current_card = player->hand[index];
	uno_deck[uno_get_random_empty_card_index()] = player->hand[index];
	player->hand[index] = NULL;
	for(int i = index; i < player->hand_count; i++)
		if(player->hand[i + 1])
			player->hand[i] = player->hand[i + 1];
	player->hand_count--;
}

int uno_get_random_empty_card_index() {
	int index;
	do {
		index = rand() % 108;
	} while(uno_deck[index]);

	return index;
}

int uno_get_random_card_index() {
	int index;
	do {
		index = rand() % 108;
	} while(!uno_deck[index]);

	return index;
}

void uno_draw_card(uno_player_t *player) {
	int index = uno_get_random_card_index();
	if(uno_deck[index]) {
		player->hand[player->hand_count++] = uno_deck[index];
		uno_deck[index] = NULL;
	}
}

void uno_add_player(char *name) {
	uno_player_t *player = safe_alloc(sizeof(uno_player_t *));
	player->name = name;
	player->hand = safe_calloc(108, sizeof(uno_card_t *));
	player->hand_count = 0;
	while(player->hand_count < 7)
		uno_draw_card(player);
	uno_players[uno_player_count++] = player;
}

void uno_remove_player(char *name) {
	for(int i = 0; i < uno_player_count; i++)
		if(!strncmp(uno_players[i]->name, name, strlen(name))) {
			for(int i1 = 0; i1 < uno_players[i]->hand_count; i1++)
				free(uno_players[i]->hand[i1]);
			free(uno_players[i]->hand);
			free(uno_players[i]);
			uno_players[i] = NULL;
			uno_player_count--;
			break;
		}
}

uno_player_t *uno_get_player(char *name) {
	for(int i = 0; i < uno_player_count; i++)
		if(!strncmp(uno_players[i]->name, name, strlen(name)))
			return uno_players[i];

	return NULL;
}

bool uno_contains_player(char *name) {
	for(int i = 0; i < uno_player_count; i++)
		if(!strncmp(uno_players[i]->name, name, strlen(name)))
			return TRUE;

	return FALSE;
}

const char *uno_color(uno_card_t *c) {
	switch(c->color) {
	case RED:
		return "Red";
	case GREEN:
		return "Green";
	case BLUE:
		return "Blue";
	case YELLOW:
		return "Yellow";
	}

	return "";
}

const char *uno_type(uno_card_t *c) {
	switch(c->type) {
	case NUMBER:
		return strformat("%d", c->value);
	case SKIP:
		return "Skip";
	case DRAW_TWO:
		return "Draw Two";
	case REVERSE:
		return "Reverse";
	case WILD:
		return "Wild";
	case WILD_DRAW_FOUR:
		return "Wild Draw Four";
	}

	return "";
}
