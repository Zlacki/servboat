include config.mk

OBJ=bin/io.o bin/irc.o bin/util.o bin/uno.o

all: mkbin options servboat

mkbin:
	@mkdir -p bin

options:
	@echo servboat build options:
	@echo "CFLAGS  = ${CFLAGS}"
	@echo "LDFLAGS = ${LDFLAGS}"
	@echo "CC      = ${CC}"

bin/%.o: src/%.c
	@echo CC $<
	@${CC} -static -o $@ -c ${CFLAGS} $<

servboat: ${OBJ}
	@echo CC -o $@
	@${CC} -o $@ ${OBJ} ${LDFLAGS}
	@echo $@ finished compiling.

clean:
	@echo cleaning
	@rm -f bin/*.o servboat
